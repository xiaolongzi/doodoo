const util = require("../../../../utils/util.js");
const app = getApp();
Page({
    data: {
        color: wx.getExtConfigSync().color
    },
    onLoad() {
        this.getCouponList();
    },
    getCouponList() {
        wx.doodoo.fetch(`/shop/api/shop/coupon/coupon`).then(res => {
            if (res && res.data.errmsg=='ok') {
                const coupon_menu = res.data.data;
                for (const item of coupon_menu) {
                    item.last_time = item.ended_at.substr(0,10);
                    item.start_time = item.started_at.substr(0,10);
                }
                let length = 2; // 默认设置两个数字宽度 22 50 86 75
                if(coupon_menu.length) {
                    coupon_menu.sort((a, b) => {
                        if(a.price&&b.price) {
                            return a.price < b.price;   // 从大到小排列
                        }
                    })
                    length = String(coupon_menu[0].price).length;
                }
                console.log(coupon_menu)
                console.log(length)
                this.setData({
                    length: length,
                    coupon_menu: coupon_menu
                });
            }
        });
    },
    // 领取优惠券
    getCoupon(e) {
        wx.doodoo.fetch(`/shop/api/shop/coupon/grtCoupon?id=${e.currentTarget.dataset.item.id}`).then(res => {
            if (res && res.data.errmsg=='ok') {
                wx.showToast({
                    title: "领取成功"
                });
            } else {
                wx.showModal({
                    title: "提示",
                    content: res.data.errmsg,
                    showCancel: false
                });
            }
        });
    },
    onPullDownRefresh: function() {
        wx.stopPullDownRefresh();
        this.getCouponList();
    }
});
